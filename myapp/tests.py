from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from .views import landing, profile
from .forms import LandingForm
from .models import Landing
from selenium import webdriver
import time

# Create your tests here.


class Story6FunctionalTest(LiveServerTestCase):
    def setUp(self):
        firefox_options = Options()
        firefox_options.add_argument('--headless')
        self.browser = webdriver.Firefox(firefox_options=firefox_options)

    def tearDown(self):
        self.browser.quit()

    def test_add_status(self):
        self.browser.get(self.live_server_url)
        # time.sleep(3)
        status = self.browser.find_element_by_id('stats')
        submit = self.browser.find_element_by_id('soubmit')
        status.send_keys('Coba Coba')
        time.sleep(3)
        submit.send_keys(Keys.RETURN)

        self.assertIn('Landing Page', self.browser.title)
        time.sleep(3)
        self.browser.get(self.live_server_url)
        self.assertIn('Coba Coba', self.browser.page_source)
        self.tearDown()

    def test_cek_title(self):
        self.browser.get(self.live_server_url)
        self.assertIn('Landing Page', self.browser.title)
        self.tearDown()

    def test_profile_button_exist(self):
        self.browser.get(self.live_server_url)
        profile_btn = self.browser.find_element_by_tag_name('button')
        self.assertIn(profile_btn.text, "Status")
        self.tearDown()

    def test_css_background(self):
        self.browser.get(self.live_server_url)
        element = self.browser.find_element_by_css_selector('body')
        cssprop = element.value_of_css_property('background-color')
        self.assertEqual(cssprop, "rgb(0, 255, 255)")
        self.tearDown()

    def test_button_color(self):
        self.browser.get(self.live_server_url)
        profile_btn = self.browser.find_element_by_id('profile')
        cssprop = profile_btn.value_of_css_property('background-color')
        self.assertEqual(cssprop, 'rgb(0, 123, 255)')
        self.tearDown()

    def test_pindah_page_profile_dan_ganti_tema(self):
        self.browser.get(self.live_server_url + "/profile")
        body = self.browser.find_element_by_tag_name('body')
        bgbody = body.value_of_css_property('background-color')
        time.sleep(3)
        self.assertEqual(bgbody, 'rgb(0, 255, 255)')
        changeThemebtn = self.browser.find_element_by_id("darkTheme")
        changeThemebtn.send_keys(Keys.RETURN)
        bgbodyNow = self.browser.find_element_by_tag_name('body').value_of_css_property('background-color')
        time.sleep(3)
        self.assertEqual(bgbodyNow, 'rgb(34, 47, 62)')
        self.tearDown()

    def test_accordion(self):
        self.browser.get(self.live_server_url + "/profile")
        self.assertIn("PMB 2018 - Dokumentasi", self.browser.page_source)
        accordion = self.browser.find_element_by_id("organisasi")
        accordion.send_keys(Keys.RETURN)
        self.tearDown()


class Story6Test(TestCase):
    def test_url_exist(self):
        response = Client().get('/landing')
        self.assertEqual(response.status_code, 200)

    def test_function_landing(self):
        found = resolve('/landing')
        self.assertEqual(found.func, landing)

    def test_template_of_landing(self):
        response = Client().get('/landing')
        self.assertTemplateUsed(response, 'landing.html')

    def test_add_new_status(self):
        status = Landing.objects.create(status='Gangerti PPW')
        count_added_status = Landing.objects.all().count()
        self.assertEqual(count_added_status, 1)

    def test_submit_empty_form(self):
        form = LandingForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ["This field is required."])

    def test_submit_form_success_and_render(self):
        status = 'Lagi Butuh Cinta'
        response = Client().post('/landing', {'status': status})
        self.assertEqual(response.status_code, 302)

        response = Client().get('/landing')
        html_response = response.content.decode('utf8')
        self.assertIn(status, html_response)

    def test_url_profile_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)

    def test_func_profile_run(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)

    def test_template_profile(self):
        response = Client().get('/profile')
        self.assertTemplateUsed(response, 'profile.html')
