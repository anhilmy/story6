var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}

$(function(){
    $('#darkTheme').on('click', function() {
        $('#body').css({
            background:"#222f3e",
        });
        $('.accordion').css({
            background:"#2F4F4F",
            hover:"#8FDADF",
        });
        $('.panel').css({
            background:"#D3D3D3",
        });

        $('article').css({
            color: 'white',
        });
    });

    $('#blueTheme').on('click', function() {
        $('#body').css({
            background:"#00FFFF",
        });
        $('.accordion').css({
            background:"#40E0D0",
        });
        $('.panel').css({
            background:"#AFEEEE",
        });
        $('article').css({
            color: 'black',
        });
    });

});
